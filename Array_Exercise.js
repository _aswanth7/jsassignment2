


function each(elements, cb) {
  
  if(elements === null || cb === undefined) return [];
  else{
    for(let i=0;i<elements.length;i++){
      cb(elements[i],i);
    }
  }
}

function map(elements, cb) {

  let resultArray = [];
  if(elements === null || cb === undefined) return []
  else{
    for(let i=0;i<elements.length;i++){
      resultArray.push(cb(elements[i]));
    }
    return resultArray;
  }
}

function reduce(elements, cb, startingValue) { 
  if(elements === null || cb === undefined) return []
  else{
    let result = startingValue? startingValue : elements[0];

    for(let i=0;i<elements.length;i++){
      result =cb(result,elements[i]);
    }
    return result;
  }
}

function find(elements, cb) {
  if(elements === null || cb === undefined) return undefined
  else{
    for(let i=0;i<elements.length;i++){
      if (cb(elements[i])) return elements[i];
    }
    return undefined;
  }
}

function filter(elements, cb) {
  let resultArray = [];
  if(elements === null || cb === undefined) return []
  else{
    for(let i=0;i<elements.length;i++){
      if (cb(elements[i])) resultArray.push(elements[i]);
    }
    return resultArray;
  }
}

function flatten(elements,resultArray = []) {
  if(elements === undefined) return [];
    for(let i=0;i<elements.length;i++){
      if (!Array.isArray(elements[i]))  resultArray.push(elements[i]);
      else flatten(elements[i],resultArray);
    }  
  return resultArray;
  }

module.exports = {each,map,reduce,find,filter,flatten};
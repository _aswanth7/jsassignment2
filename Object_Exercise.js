

function keys(obj) {
    let resultArray = [];
    if(obj === undefined) return [];
    else{
        for(let key in obj){

       resultArray.push(key);
        }
    return resultArray;
    }
  }

  function values(obj) {
    let resultArray = [];
    if(obj === undefined) return [];
    else{
        for(let key in obj){
            resultArray.push(obj[key]);
        }
    return resultArray;
    }
  }

  function mapObject(obj, cb) {
    let resultArray = [];
    if(obj === undefined || cb === undefined) return [];
    else{
       let objectValues= values(obj);
       for(let i=0;i<objectValues.length;i++)  resultArray.push(cb(objectValues[i]));
       return resultArray;
    }
  }

function pairs(obj) {
    let resultArray = [];
    if(obj === undefined) return [];
    else{
        for(let key in obj){
            resultArray.push([key,obj[key]]);
        }
    return resultArray;
    }
}

function invert(obj) {
    let resultObject = {};
    if(obj === undefined) return {};
    else{
        for(let key in obj){
            resultObject[obj[key]]= key;
        }
    return resultObject;
    }
}
  
function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    if(obj === undefined || defaultProps === undefined) return {};
    else{
        let objectKeys = keys(obj);
        for(let key in defaultProps) {
            if(!objectKeys.includes(key))obj[key] = defaultProps[key];
        }
        return obj;
    }
  }


  module.exports = { keys,values,mapObject,pairs,invert,defaults}
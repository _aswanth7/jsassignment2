


function counterFactory() {
    let counter = 0;
    return {
        increment : function () { counter++; return counter;},
        decrement : function () { counter--; return counter;}
    };
}

function limitFunctionCallCount(cb, n) {
    return function (cbArg){
        while(n){
            cb(cbArg);
            n--;
        }
    }
}

function cacheFunction(cb) {
    let cache = {};
    return function(cbArg){
        if(cbArg in cache)  return cache[cbArg];
        else{
            let cbResult = cb(cbArg);
            cache[cbArg] = cbResult;
            //console.log("arg added to cache "+JSON.stringify(cache));
            return cbResult;
        }
    }
}


  module.exports ={counterFactory,limitFunctionCallCount,cacheFunction};
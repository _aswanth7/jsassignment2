const closureModule = require("./Closures_Exercise");



let counterFactory = closureModule.counterFactory();

// console.log(counterFactory.increment());
// console.log(counterFactory.increment());
// console.log(counterFactory.increment());
// console.log(counterFactory.decrement());

// console.log(closureModule.counterFactory().increment());
// console.log(closureModule.counterFactory().decrement());

// const test= function(n){
//     console.log("Hello World "+n);
// }
// const limitFunctionCall =closureModule.limitFunctionCallCount(test,5);
// limitFunctionCall(5);

const test1= function(n){
   return n*n;
}
const cacheFunction = closureModule.cacheFunction(test1);
console.log(cacheFunction(5));
console.log(cacheFunction(10));
console.log(cacheFunction(5));
console.log(cacheFunction(10));